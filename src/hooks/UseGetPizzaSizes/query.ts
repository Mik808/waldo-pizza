import { gql } from '@apollo/client'

export const GET_PIZZA_SIZES = gql`
  query getPizzaSizes {
    pizzaSizes {
        name,
        maxToppings, 
        basePrice, 
        toppings {
        topping {
          name,
          price
        }
      }
    }
  }
`
