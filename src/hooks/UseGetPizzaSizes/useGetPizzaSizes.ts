import { useQuery } from '@apollo/client'
import { useState, useEffect } from 'react'
import { GET_PIZZA_SIZES } from './query'
import { PizzasDB, Pizzas, ToppingWrapper } from '../../types/generalTypes'

export default function useGetPizzaSizes() {
  const [pizzaSizes, setPizzaSizes] = useState<Pizzas>()
  const [toppings, setToppings] = useState<ToppingWrapper[]>()
  const { data, loading, error } = useQuery<PizzasDB>(GET_PIZZA_SIZES)

  useEffect(() => {
    if (data) {
      setPizzaSizes(data.pizzaSizes)
      setToppings(data.pizzaSizes[0].toppings)
    }
  }, [data])

  return {
    pizzaSizes, toppings, loading, error,
  }
}
