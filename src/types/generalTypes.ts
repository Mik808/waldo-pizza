export interface ToppingBackend {
  name: string
  price: number
  __typename: string
}

export interface ToppingWrapper {
  __typename: string
  topping: ToppingBackend
}

export interface Pizza {
  id: number,
  size: string,
  toppings: number,
  quantity: number
}

export interface Pizzas extends Array<PizzaDB> { }

export type ChangeEInput = React.ChangeEvent<HTMLInputElement>
export type ChangeESelect = React.ChangeEvent<HTMLSelectElement>

export interface PizzasDB {
  pizzaSizes: PizzaDB[]
}

export interface PizzaDB {
  name: string,
  maxToppings: number,
  basePrice: number,
  toppings: ToppingWrapper[] | undefined,
  __typename: string
}

export interface ToppingDB {
  name: string
  price: number
}

export interface Toppings extends Array<ToppingDB> { }

export interface PizzaSize {
  name: string | undefined,
  maxToppings: number | null | undefined,
  basePrice: number | undefined,
}
