import React from 'react'
import OrderForm from '../OrderForm'
import { CartProvider } from '../CartContext'
import Cart from '../Cart'

function App() {
  return (
    <div>
      <CartProvider>
        <OrderForm />
        <Cart />
      </CartProvider>
    </div>
  )
}

export default App
