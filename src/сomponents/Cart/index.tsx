import React from 'react'
import { CartItem } from './interface'
import {
  StyledCartItem, StyledPizzaName, StyledCartItems, StyledTotal,
} from './style'
import { ToppingDB } from '../../types/generalTypes'
import { useCart } from '../CartContext'

export default function Cart() {
  const { cartState, handleRemove } = useCart()

  let total = 0
  if (cartState.length) {
    const pizzaPrices = cartState.map((pizza) => pizza.price)
    total = pizzaPrices.reduce((acc, curValue) => acc + curValue)
    total = Math.round(total * 100) / 100
  }

  return (
    <div>
      <StyledTotal>
        <b>Total: </b>
        <span>{total}</span>
      </StyledTotal>
      <StyledCartItems>
        {cartState && cartState.map((pizza: CartItem, i: number) => (
          <StyledCartItem key={i + Date.now()}>
            <div>
              Pizza #
              {i + 1}
            </div>
            <StyledPizzaName>{pizza.pizzaSize.name}</StyledPizzaName>
            <div>
              {pizza.toppings.map((topping: ToppingDB, j: number) => (
                <div key={j}>
                  <div>{topping.name}</div>
                </div>
              ))}
            </div>
            <button type="button" onClick={() => handleRemove(pizza.id)}>Remove</button>
          </StyledCartItem>
        ))}
      </StyledCartItems>
    </div>
  )
}
