import { PizzaSize } from '../../types/generalTypes'
import { Topping } from '../SelectToppings/interface'

export interface CartTopping {
  name: string,
  price: number
}

export interface CartItemProps {
  pizzaSize: PizzaSize
  toppings: Topping[]
  price: number
}

export interface CartItem {
  id: string
  pizzaSize: PizzaSize
  toppings: Topping[]
  price: number
}
