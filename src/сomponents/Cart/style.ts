import styled from 'styled-components'

export const StyledCartItems = styled.div`
  display: flex;
  flex-direction: row; 
  flex-wrap: wrap;
  width: 1000px;
`

export const StyledCartItem = styled.div`
  margin-right: 20px;
  margin-bottom: 20px;
`

export const StyledAddToCartBtn = styled.button`
  margin-bottom: 20px;
`

export const StyledPizzaName = styled.div`
  margin-bottom: 6px;
`

export const StyledTotal = styled.div`
  margin-top: 20px;
  margin-bottom: 8px;
`
