import React, { ReactNode, useContext, useState } from 'react'
import { CartItem, CartItemProps } from '../Cart/interface'
import { CartContextInterface } from './interface'

const initialState = {
  cartState: [],
  handleAdd: () => null,
  handleRemove: () => null,
}

const CartContext = React.createContext<CartContextInterface>(initialState)

export const useCart = () => useContext(CartContext)

export function CartProvider({ children }: { children: ReactNode }) {
  const [cartState, setCartState] = useState<CartItem[]>([])

  const handleAdd = (pizzaItem: CartItemProps) => {
    const id = Math.random().toString(16).slice(2)
    setCartState([...cartState, { ...pizzaItem, id }])
  }

  const handleRemove = (id: string) => {
    const modifiedState = cartState.filter((pizza) => pizza.id !== id)
    setCartState(modifiedState)
  }

  return (
    <CartContext.Provider value={{
      cartState,
      handleAdd,
      handleRemove,
    }}
    >
      {children}
    </CartContext.Provider>
  )
}
