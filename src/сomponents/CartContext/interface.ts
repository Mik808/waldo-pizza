import { CartItem, CartItemProps } from '../Cart/interface'

export interface CartContextInterface {
  cartState: CartItem[],
  handleAdd: (pizzaItem: CartItemProps) => void,
  handleRemove: (id:string) => void,
}
