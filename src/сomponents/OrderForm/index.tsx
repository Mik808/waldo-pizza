import React from 'react'
import SelectPizzaSize from '../SelectPizzaSize'
import useGetPizzaSizes from '../../hooks/UseGetPizzaSizes/useGetPizzaSizes'

export default function OrderForm() {
  const { pizzaSizes, toppings } = useGetPizzaSizes()

  return (
    <div>
      <SelectPizzaSize pizzaSizes={pizzaSizes || null} toppings={toppings} />
    </div>
  )
}
