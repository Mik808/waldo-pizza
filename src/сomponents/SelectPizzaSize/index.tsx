import React, { useEffect, useState } from 'react'
import { SelectPizzaSizeProps } from './interface'
import { StyledSelect } from './style'
import { ChangeESelect, PizzaSize } from '../../types/generalTypes'
import SelectToppings from '../SelectToppings'

export default function SelectPizzaSize(props: SelectPizzaSizeProps) {
  const { pizzaSizes, toppings } = props

  const [pizzaSizeData, setPizzaSizeData] = useState<PizzaSize>({
    name: '',
    maxToppings: undefined,
    basePrice: undefined,
  })
  const [pizzaSize, setPizzaSize] = useState<string>('')

  useEffect(() => {
    if (pizzaSizes) {
      const pizzaSizeObj = {
        name: pizzaSizes[0].name,
        maxToppings: pizzaSizes[0].maxToppings,
        basePrice: pizzaSizes[0].basePrice,
      }
      setPizzaSizeData(pizzaSizeObj)
      setPizzaSize(pizzaSizes[0].name)
    }
  }, [pizzaSizes])

  const handleSelect = (e: ChangeESelect) => {
    const pizza = pizzaSizes?.find((pizzaEl) => pizzaEl.name === e.target.value)
    const pizzaSizeObj = {
      name: pizza?.name,
      maxToppings: pizza?.maxToppings,
      basePrice: pizza?.basePrice,
    }
    setPizzaSizeData(pizzaSizeObj)
    setPizzaSize(e.target.value)
  }

  return (
    <StyledSelect>
      <label htmlFor="pizzaSize">Select Pizza Size</label>
      <select name="pizzaSize" id="pizzaSize" onChange={handleSelect} value={pizzaSize}>
        {pizzaSizes?.map((pizza) => (
          <option key={pizza.name + Date.now()} value={pizza.name}>
            {`${pizza.name} ${pizza.basePrice}`}
          </option>
        ))}
      </select>
      <SelectToppings toppingList={toppings} pizzaSize={pizzaSizeData} />
    </StyledSelect>
  )
}
