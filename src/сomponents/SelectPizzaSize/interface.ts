import { Pizzas, ToppingWrapper } from '../../types/generalTypes'

export interface SelectPizzaSizeProps {
  pizzaSizes: Pizzas | null
  toppings: ToppingWrapper[] | undefined
}
