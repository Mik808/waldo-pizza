export const SET_CHECKED = 'SET_CHECKED'
export const SET_INITIAL_STATE = 'SET_INITIAL_STATE'
export const SET_ACTIVE = 'SET_ACTIVE'
export const RESELECT = 'RESELECT'
export const SET_MAX_TOPPINGS = 'SET_MAX_TOPPINGS'
export const SET_ORDER = 'SET_ORDER'
