import React from 'react'
import { StyledCheckBoxes, StyledLabel } from './style'
import useSetToppings from './useSetToppings'
import { SelectToppingsProps, SelectedTopping } from './interface'
import { CartTopping } from '../Cart/interface'
import { ChangeEInput } from '../../types/generalTypes'
import { useCart } from '../CartContext'

export default function SelectToppings(props: SelectToppingsProps) {
  const { toppingList, pizzaSize } = props
  const { handleAdd } = useCart()

  const maxToppings = pizzaSize?.maxToppings === null ? toppingList?.length : pizzaSize?.maxToppings

  const { toppingsState, handleChange } = useSetToppings(toppingList, maxToppings)

  const checkedToppings = toppingsState.toppings.filter((topping: SelectedTopping) => (
    topping.checked))

  const cartToppings = checkedToppings.map((topping: SelectedTopping) => (
    { name: topping.name, price: topping.price }))

  let toppingsTotal: number = 0
  if (checkedToppings.length) {
    const toppingsPrices = checkedToppings.map((topping: CartTopping) => topping.price)
    toppingsTotal = toppingsPrices.reduce((acc, curValue) => acc + curValue)
  }
  const basePrice = pizzaSize.basePrice ? pizzaSize.basePrice : 0
  let pizzaPrice = basePrice + toppingsTotal
  pizzaPrice = Math.round(pizzaPrice * 100) / 100

  return (
    <div>
      <StyledCheckBoxes>
        {toppingsState.toppings.map((topping: SelectedTopping, i: number) => (
          <StyledLabel key={i} htmlFor={topping.name} inactive={topping.inactive}>
            {topping.name}
            <input type="checkbox" name={topping.name} id={topping.name} checked={topping.checked} onChange={(e: ChangeEInput) => handleChange(e, topping)} />
            {topping.price}
          </StyledLabel>
        ))}
      </StyledCheckBoxes>
      <div>
        Pizza Price:
        <span>{pizzaPrice}</span>
      </div>
      <button
        type="button"
        onClick={() => handleAdd({
          toppings: cartToppings,
          pizzaSize,
          price: pizzaPrice,
        })}
      >
        Add to Cart
      </button>
    </div>
  )
}
