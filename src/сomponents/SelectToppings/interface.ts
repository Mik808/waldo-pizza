import { PizzaSize, ToppingWrapper, ToppingBackend } from '../../types/generalTypes'

export interface Topping {
  name: string
  price: number
}

export interface Toppings extends Array<Topping> { }

export interface Label {
  inactive: Boolean
}
export interface SelectedTopping {
  name: string
  checked: boolean
  inactive: boolean
  price: number
}

export interface SelectedToppings {
  toppings: SelectedTopping[],
  checkedLength: number,
  maxToppings: MaxToppings,
  order: (string | null)[]
}

export type MaxToppings = number | null | undefined
export interface SetChecked {
  topping: string | null,
  checked: Boolean
}

export type ActionType =
  | { type: 'SET_CHECKED'; payload: SetChecked }
  | { type: 'SET_ACTIVE'; payload: string | null }
  | { type: 'SET_INITIAL_STATE'; payload: SelectedToppings }
  | { type: 'SET_MAX_TOPPINGS'; payload: MaxToppings }
  | { type: 'RESELECT'; payload: SelectedTopping }
  | { type: 'SET_ORDER'; payload: { toppingName: string | null, checked: Boolean } }

export interface SelectToppingsProps {
  toppingList: ToppingWrapper[] | undefined
  pizzaSize: PizzaSize
}

export interface ToppingsListProp {
  __typename: string,
  topping: ToppingBackend
}
