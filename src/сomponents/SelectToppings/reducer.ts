import { SelectedTopping, SelectedToppings, ActionType } from './interface'

export const reducer = (state: SelectedToppings, action: ActionType) => {
  switch (action.type) {
    case 'SET_CHECKED':
      return {
        ...state,
        toppings: state.toppings.map((topping: SelectedTopping) => {
          if (action.payload.topping === topping.name) {
            return { ...topping, checked: !topping.checked }
          }
          return topping
        }),
        checkedLength: action.payload.checked ? state.checkedLength + 1 : state.checkedLength - 1,
      }
    case 'SET_INITIAL_STATE':
      return action.payload
    case 'SET_MAX_TOPPINGS':
      return {
        ...state,
        maxToppings: action.payload,
        toppings: state.toppings.map((topping: SelectedTopping) => (
          { ...topping, inactive: false, checked: false })),
        order: [],
      }
    case 'SET_ACTIVE':
      const selected = state.toppings.filter((topping: SelectedTopping) => topping.checked === true)
      const selectedLength = selected.length
      const stateMaxToppings = state.maxToppings ? state.maxToppings : 0
      if (stateMaxToppings <= selectedLength) {
        return {
          ...state,
          toppings: state.toppings.map((topping: SelectedTopping) => {
            if (!topping.checked) {
              return { ...topping, inactive: true }
            }
            return topping
          }),
        }
      }
      return {
        ...state,
        toppings: state.toppings.map(
          (topping: SelectedTopping) => ({ ...topping, inactive: false }),
        ),
      }
    case 'SET_ORDER':
      return {
        ...state,
        order: action.payload.checked ? [...state.order, action.payload.toppingName]
          : state.order.filter((toppingName: string | null) => (
            action.payload.toppingName !== toppingName)),
      }
    case 'RESELECT':
      const unChecked = state.toppings.find((topping: SelectedTopping) => (
        topping.name === state.order[0]))
      return {
        ...state,
        toppings: state.toppings.map((topping: SelectedTopping) => {
          if (topping.name === action.payload.name) {
            return { ...topping, checked: true, inactive: false }
          }
          if (topping.name === unChecked?.name) {
            return { ...topping, checked: false, inactive: true }
          }
          return topping
        }),
        order: state.order.filter((toppingName: string | null) => (
          unChecked?.name !== toppingName)),
      }
    default:
      return state
  }
}
