import styled from 'styled-components'
import { Label } from './interface'

export const StyledCheckBoxes = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
  margin-bottom: 20px;
`
export const StyledLabel = styled.label`
  color: ${(props:Label) => (props.inactive ? 'grey' : '#000')}
`
