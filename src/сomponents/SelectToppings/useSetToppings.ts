import {
  useEffect, useReducer, useState,
} from 'react'
import {
  SelectedTopping, SelectedToppings, MaxToppings,
} from './interface'
import { ToppingWrapper, ChangeEInput } from '../../types/generalTypes'
import { reducer } from './reducer'
import * as types from './actionTypes'

export default function useSetToppings(
  toppingsListProp: ToppingWrapper[] | undefined,
  maxToppingsProp: MaxToppings,
) {
  const toppingsList = toppingsListProp
  const [maxToppings, setMaxToppings] = useState(maxToppingsProp)

  const initialToppings: SelectedToppings = {
    toppings: [],
    checkedLength: 0,
    maxToppings,
    order: [],
  }
  toppingsList?.forEach((topping: ToppingWrapper) => {
    initialToppings.toppings.push({
      name: topping.topping.name,
      checked: false,
      inactive: false,
      price: topping.topping.price,
    })
  })

  const [toppingsState, dispatch] = useReducer(reducer, initialToppings)

  useEffect(() => {
    if (toppingsList) {
      dispatch({ type: types.SET_INITIAL_STATE, payload: initialToppings })
    }
  }, [toppingsList])

  useEffect(() => {
    if (maxToppingsProp) {
      setMaxToppings(maxToppingsProp)
      dispatch({ type: types.SET_MAX_TOPPINGS, payload: maxToppingsProp })
    }
  }, [maxToppingsProp])

  const handleChange = (e: ChangeEInput, topping: SelectedTopping) => {
    const toppingName = e.target.getAttribute('name')
    const { checked } = e.target

    dispatch({ type: types.SET_ORDER, payload: { toppingName, checked } })

    if (topping.inactive) {
      dispatch({ type: types.RESELECT, payload: topping })
    } else {
      dispatch({
        type: types.SET_CHECKED,
        payload: {
          topping: toppingName,
          checked: e.target.checked,
        },
      })

      dispatch({ type: types.SET_ACTIVE, payload: toppingName })
    }
  }

  return { toppingsState, handleChange }
}
